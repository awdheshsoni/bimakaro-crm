<?php

/*
/*
 Created By : Awdhesh
 Created Date: 14/07/2020
 Class: Lead 
*/

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use  App\Lead;

class LeadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //validate incoming request 
        $this->validate($request, [
            'annual_income' => 'required|numeric',
        ]);

        
        $input = $request->all();
        $lead = new Lead;
        // Save the Dynamic value in Leads table
        try {
            foreach($input as $key => $value) {
                    $lead->$key = $value;
            }       
            $lead->deleted = 0;
            $lead->date_entered = date("Y-m-d H:i:s");
            $lead->date_modified = date("Y-m-d H:i:s");;
            $lead->save();     
           //return successful response
           return response()->json(['lead' => $lead, 'message' => ' Lead Created'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => $e], 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lead  $Lead
     * @return \Illuminate\Http\Response
     */
    public function show(Lead $lead)
    {
        try {
            
            $lead = Lead::all();

            return response()->json(['lead' => $lead], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'lead not found!'], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lead  $Lead
     * @return \Illuminate\Http\Response
     */
    public function edit(Lead $lead)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lead $lead)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lead $lead)
    {
        //
    }


}
