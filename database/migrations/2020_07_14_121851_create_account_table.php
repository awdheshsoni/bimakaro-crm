<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',150);
            $table->dateTime('date_entered')->nullable();
            $table->dateTime('date_modified')->nullable();
            $table->integer('modified_user_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('deleted');
            $table->integer('assigned_user_id')->nullable();
            $table->string('account_type',50)->nullable();
            $table->string('industry',50)->nullable();
            $table->string('annual_revenue',100)->nullable();
            $table->string('phone_fax',100)->nullable();
            $table->string('billing_address_street',150)->nullable();
            $table->string('billing_address_city',150)->nullable();
            $table->string('billing_address_state',150)->nullable();
            $table->string('billing_address_postalcode',20)->nullable();
            $table->string('billing_address_country',150)->nullable();
            $table->string('rating',150)->nullable();
            $table->string('phone_office',150)->nullable();
            $table->string('phone_alternate',150)->nullable();
            $table->string('website',150)->nullable();
            $table->string('ownership',100)->nullable();
            $table->string('employees',150)->nullable();
            $table->string('ticker_symbol',150)->nullable();
            $table->string('shipping_address_street',150)->nullable();
            $table->string('shipping_address_city',150)->nullable();
            $table->string('shipping_address_state',150)->nullable();
            $table->string('shipping_address_postalcode',20)->nullable();
            $table->string('shipping_address_country',150)->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('sic_code',10)->nullable();
            $table->integer('campaign_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
