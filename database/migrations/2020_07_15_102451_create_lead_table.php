<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',150);
            $table->dateTime('date_entered')->nullable();
            $table->dateTime('date_modified')->nullable();
            $table->integer('modified_user_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('deleted')->nullable();
            $table->integer('assigned_user_id')->nullable();
            $table->string('salutation',255)->nullable();
            $table->string('first_name',100)->nullable();
            $table->string('last_name',100)->nullable();
            $table->string('title',100)->nullable();
            $table->string('service_type',100)->nullable();
            $table->string('gender',100)->nullable();
            $table->string('life_stage',100)->nullable();
            $table->string('utm_campaign',100)->nullable();
            $table->string('utm_source',100)->nullable();
            $table->string('utm_medium',100)->nullable();
            $table->string('utm_term',100)->nullable();
            $table->string('utm_content',100)->nullable();
            $table->integer('annual_income')->nullable();
            $table->string('user_ip',100)->nullable();
            $table->string('browser',100)->nullable();
            $table->string('device',100)->nullable();
            $table->string('soucre',100)->nullable();
            $table->string('device_model',100)->nullable();
            $table->string('education',100)->nullable();
            $table->string('gclid_field',100)->nullable();
            $table->string('photo',100)->nullable();
            $table->string('department',100)->nullable();
            $table->string('phone_home',100)->nullable();
            $table->string('phone_mobile',100)->nullable();
            $table->string('phone_work',100)->nullable();
            $table->string('phone_other',100)->nullable();
            $table->string('phone_fax',100)->nullable();
            $table->text('lawful_basis')->nullable();
            $table->tinyInteger('do_not_call')->nullable();
            $table->date('date_reviewed')->nullable();
            $table->string('lawful_basis_source',100)->nullable();
            $table->string('primary_address_street',150)->nullable();
            $table->string('primary_address_city',150)->nullable();
            $table->string('primary_address_state',150)->nullable();
            $table->string('primary_address_postalcode',20)->nullable();
            $table->string('primary_address_country',150)->nullable();
            $table->string('alt_address_street',150)->nullable();
            $table->string('alt_address_city',150)->nullable();
            $table->string('alt_address_state',150)->nullable();
            $table->string('alt_address_postalcode',20)->nullable();
            $table->string('alt_address_country',150)->nullable();
            $table->string('assistant',75)->nullable();
            $table->string('assistant_phone',100)->nullable();
            $table->tinyInteger('converted')->nullable();
            $table->string('refered_by',100)->nullable();
            $table->string('lead_source',75)->nullable();
            $table->text('lead_source_description')->nullable();
            $table->string('status',100)->nullable();
            $table->text('status_description')->nullable();
            $table->string('reports_to_id',100)->nullable();
            $table->string('account_name',255)->nullable();
            $table->text('account_description')->nullable();
            $table->integer('contact_id')->nullable();
            $table->integer('account_id')->nullable();
            $table->integer('campaign_id')->nullable();
            $table->date('birthdate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads')->nullable();
    }
}
