<?php

use Illuminate\Database\Seeder;

class LeadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::table('leads')->get()->count() == 0){

            DB::table('leads')->insert([
                [
                    "name"=> "Google",
					"first_name"=> "Awdhesh",
					"last_name"=> "Soni",
					"assigned_user_id"=> "1",
					"gender"=> "M",
					"life_stage"=> "Single (Independent)",
					"phone_home"=> "9383838474",
					"service_type"=> "Term",
					"utm_campaign"=> "Term Insurance",
					"utm_source"=> "Facebook",
					"utm_medium"=> "contet",
					"utm_term"=> "term insurance plan",
					"utm_content"=> "l dec 2019",
					"annual_income"=> "500000",
					"education"=> "Graduate",
					"deleted"=> 0,
					"date_entered"=> "2020-07-24 06:58:00",
					"date_modified"=> "2020-07-24 06:58:00",
					"updated_at"=> "2020-07-24T06:58:00.000000Z",
					"created_at"=> "2020-07-24T06:58:00.000000Z",
                ],
                [
                    "name"=> "Facebook",
					"first_name"=> "Saaz",
					"last_name"=> "Rai",
					"assigned_user_id"=> "1",
					"gender"=> "M",
					"life_stage"=> "Single (Independent)",
					"phone_home"=> "9383838474",
					"service_type"=> "Term",
					"utm_campaign"=> "Term Insurance",
					"utm_source"=> "Facebook",
					"utm_medium"=> "contet",
					"utm_term"=> "term insurance plan",
					"utm_content"=> "l dec 2019",
					"annual_income"=> "500000",
					"education"=> "Graduate",
					"deleted"=> 0,
					"date_entered"=> "2020-07-24 06:58:00",
					"date_modified"=> "2020-07-24 06:58:00",
					"updated_at"=> "2020-07-24T06:58:00.000000Z",
					"created_at"=> "2020-07-24T06:58:00.000000Z",
                ],
                [
                    "name"=> "Linken",
					"first_name"=> "Shanti",
					"last_name"=> "Gola",
					"assigned_user_id"=> "1",
					"gender"=> "M",
					"life_stage"=> "Single (Independent)",
					"phone_home"=> "9383838474",
					"service_type"=> "Term",
					"utm_campaign"=> "Term Insurance",
					"utm_source"=> "Facebook",
					"utm_medium"=> "contet",
					"utm_term"=> "term insurance plan",
					"utm_content"=> "l dec 2019",
					"annual_income"=> "500000",
					"education"=> "Graduate",
					"deleted"=> 0,
					"date_entered"=> "2020-07-24 06:58:00",
					"date_modified"=> "2020-07-24 06:58:00",
					"updated_at"=> "2020-07-24T06:58:00.000000Z",
					"created_at"=> "2020-07-24T06:58:00.000000Z"
                ]

            ]);

        } else { echo "\e[31mTable is not empty, therefore NOT "; }

    }
    }
}
